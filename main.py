from actortypes import PC, NPC, MOB, Turn, Round
import os

# KEYS
# [TYPE,    NAME,    ROLL,    MODIFIER]
# TYPE: PC, NPC, MOB
# NAME: actor name
# ROLL: if int, the this is the roll+mod. If False, then the rng rolls
#       MOBs can't set individual rolls, so this key is interpreted as
#       the number of monsters in the mob
# MODIFIER: the modifier to initiative. Useful if you let the rng to
#       make the roll for you. All the monsters in MOB have the same
#       modifier.

pc_list = [
    PC("Xhin-Hao", roll=1, mod=2, ),
    PC("Frosky",   roll=1, mod=2, ),
    PC("Jesen",    roll=1, mod=2, ),
    PC("Sandor",   roll=1, mod=2, ),
]

alist = [

    ["PC", "Xhin-Hao", 20, 0],
    ["PC", "Frosky",   18, 0],
    ["PC", "Jesen",    9, 0],
    ["PC", "Sandor",   16, 0],

    # ["NPC", "Xander", False, 3],
    # ["NPC", "Myles", False, 2],
    # ["NPC", "Sammy", False, 3],
    # ["NPC", "Caprice", False, 2],
    # ["NPC", "Jonas", False, 3],

    ["MOB", "Zombi", 12, -2],
    ["MOB", "Wight", 4, 2],
]


def main():

    t = Turn(alist)
    for pc in pc_list:
        t.add(pc)
    t.sort_initiative()

    round_ = Round(t)
    round_.cli()


if __name__ == "__main__":
    main()
