import random
import os
import sys


class Dice():
    def __init__(self, number, faces):
        self.number = number
        self.faces = faces

    def roll(self):
        return [random.randint(1, self.faces) for i in range(self.number)]


class Attack():
    def __init__(self, to_hit, dice):
        """docstring for __init__"""
        if not isinstance(dice, Dice):
            raise TypeError("`dice` must be of type `Dice`")
        self.to_hit = to_hit
        self.dice = dice

    def roll(self):
        roll_ = sum(Dice(1, 20).roll())

        if roll_ == 20:
            attack_ = "CRIT"
            dmage_ = self.dice.faces*self.dice.number + sum(self.dice.roll())
        else:
            attack_ = roll_ + self.to_hit
            dmage_ = sum(self.dice.roll())

        return attack_, dmage_


class Actor():

    def roll_d20():
        """ Rolls a d20"""
        return random.randint(1, 20)

    def __init__(self, name, roll=False, mod=0, armor_class=10,
                 attack=Attack(0, Dice(1, 6)), damage=0):
        self.modifier = int(mod)
        self.initiative = 0
        self.name = name
        self.roll = roll
        self.reaction = True
        self.armor_class = armor_class
        self.damage = damage
        self.__attack = attack

    def roll_initiative(self):
        """Rolls initiative if False, if not, uses given value."""
        if self.roll:
            self.initiative = int(self.roll)
        else:
            self.initiative = Actor.roll_d20() + self.modifier

    def attack(self):
        """ Performs an attack roll """
        return self.__attack.roll()

    def react(self):
        """ Expends reaction"""
        if self.reaction:
            self.reaction = False
        else:
            print("No more reactions for {}".format(self.name))

    def recover_reaction(self):
        """ Recovers reaction """
        self.reaction = True

    def print_reaction(self):
        """ Prints reaction marker """
        if self.reaction:
            react = ""
        else:
            react = "*"
        return react

    def p_ac(self):
        """docstring for p_name"""
        return f"[{self.armor_class}]"

    def p_name(self):
        """docstring for p_name"""
        return f"{self.name}"

    def p_damage(self):
        """docstring for p_damage"""
        return f"{{{self.damage}}}"

    def __repr__(self):
        """ String representation of actor """
        line = f"{self.name}\t {{{self.damage}}} [{self.armor_class}]"
        # line = f"{self.initiative}\t [{self.damage}] {self.name}"
        # line = "({0})\t [{1}] {2}".format(self.initiative, self.damage, self.name, self.modifier)
        return line


class NPC(Actor):
    """ """

    def __init__(self, *arg, **kwarg):
        super(NPC, self).__init__(*arg, **kwarg)


class MOB():
    """ """

    def __init__(self, number, *arg, **kwarg):
        group = []
        for i in range(number):
            group.append(NPC(arg[0] + '_' + str(i), mod=kwarg["mod"]))
        self.group = group


class PC(Actor):
    """ """

    def __init__(self, *arg, **kwarg):
        super(PC, self).__init__(*arg, **kwarg)


class Turn():
    """ """

    def process_actor_list(alist):
        """ """
        real_alist = []
        for elem in alist:
            cl, name, roll, mod = elem
            if cl == "PC":
                real_alist.append(PC(name, roll=roll, mod=mod))
            elif cl == "NPC":
                real_alist.append(NPC(name, roll=roll, mod=mod))
            elif cl == "MOB":
                number = roll
                for monster in MOB(number, name, mod=mod).group:
                    real_alist.append(monster)
        return real_alist

    def __init__(self, actors=None, *arg, **kwarg):

        if actors:
            self.actors = Turn.process_actor_list(actors)
            for actor in self.actors:
                actor.roll_initiative()
        self.turn = 0
        self.sort_initiative()

    def sort_initiative(self):
        self.turnorder = sorted(
            self.actors,
            key=lambda x: x.initiative,
            reverse=True)

    def next(self):
        self.turn += 1
        if self.turn == len(self.turnorder):
            self.turn = 0
            self.reset_reactions()

    def damage(self, j, d):
        self.turnorder[j].damage += d

    def kill(self, j):
        self.turnorder.pop(j)

    def add(self, actor):
        self.turnorder.append(actor)

    def reset_reactions(self):
        for a in self.turnorder:
            a.recover_reaction()

    def turn_marker(self, i):
        if i == self.turn:
            return " -> "
        else:
            return "    "

    def __repr__(self):
        columns = [[""], ["Turn"], ["R"], ["HP"], ["AC"], ["Name"], ["Attack"]]
        line = ""
        for i, actor in enumerate(self.turnorder):
            f = str(actor.attack()) if i == self.turn else ""
            columns[0] += [f"{self.turn_marker(i)}"]
            columns[1] += [f"{i:2}"]
            columns[2] += [f"{actor.print_reaction()}"]
            columns[3] += [f"{actor.p_damage()}"]
            columns[4] += [f"{actor.p_ac()}"]
            columns[5] += [f"{actor.p_name()}"]
            columns[6] += [f"{f}"]

        for j, c in enumerate(columns):
            maxlen = 0
            for entry in c:
                maxlen = len(entry) if maxlen < len(entry) else maxlen

            new_c = []
            for entry in c:
                new_c += [entry.ljust(maxlen + 3)]

            columns[j] = new_c

        lines = len(columns[0])*[""]
        for j in range(len(lines)):
            for i in range(len(columns)):
                lines[j] += columns[i][j]
        table = "\n".join(lines)

        return table


class Round():
    def __init__(self, turn, *arg, **kwarg):
        self.turn = turn

    def clear():
        os.system('cls' if os.name == 'nt' else 'clear')

    def Process(self, cmd):
        cmd = cmd.split(" ")
        if len(cmd) > 0:
            if cmd[0] == "n":
                Round.clear()
                self.turn.next()
                print(self.turn)
            elif cmd[0] == "q":
                sys.exit(0)
            elif cmd[0] == "k":
                Round.clear()
                self.turn.kill(int(cmd[1]))
                print(self.turn)
            elif cmd[0] == "d":
                Round.clear()
                self.turn.damage(int(cmd[1]), int(cmd[2]))
                print(self.turn)
            elif cmd[0] == "r":
                Round.clear()
                self.turn.turnorder[int(cmd[1])].react()
                print(self.turn)
            elif cmd[0] == "a":
                Round.clear()
                actor = Turn.process_actor_list([cmd[1:]])[0]
                self.turn.add(actor)
                print(self.turn)

    def cli(self):
        while True:
            Round.clear()
            # print("    Turn\t Init\t Dmg Name")
            print(self.turn)
            command = input(
                'Insert command, (n)ext, (q)uit, (r)eact, (d)amage # HP, (k)ill #: ')
            self.Process(command)

